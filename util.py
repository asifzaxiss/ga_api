from google.analytics.data_v1beta.types import MetricType

def general_data_reformat(response):
    
    index_keys = {"dimensions":{}, "metrics":{}}
    index = 0
    for dimensionHeader in response.dimension_headers:
        print(f"Dimension header name: {dimensionHeader.name}")
        index_keys["dimensions"][index] = dimensionHeader.name
        index += 1

    index = 0
    for metricHeader in response.metric_headers:
        metric_type = MetricType(metricHeader.type_).name
        print(f"Metric header name: {metricHeader.name} ({metric_type})")
        index_keys["metrics"][index] = metricHeader.name
        
        index += 1
    
    data = []

    for row in response.rows:
        item = {}
        for i in range(len(row.dimension_values)):
            item[index_keys["dimensions"][i]] = row.dimension_values[i].value
        for i in range(len(row.metric_values)):
            item[index_keys["metrics"][i]] = row.metric_values[i].value
        data.append(item)
    

    return data

def source_data_reformat(response):
    data = []
    # [START source_data_reformat]
    print("Report result:")
    for row in response.rows:
        dmv = row.dimension_values
        mv = row.metric_values
        item = {}
        item["country"] = dmv[0].value
        item["city"] = dmv[1].value
        item["date"] = dmv[2].value
        item["source"] = dmv[3].value
        item["users"] = mv[0].value
        item["events"] = mv[1].value
        data.append(item)
    # [END source_data_reformat]

    return data


def events_data_reformat(response):
    data = []
    # [START events_data_reformat]
    print("Report result:")
    for row in response.rows:
        dmv = row.dimension_values
        mv = row.metric_values
        item = {}
        item["country"] = dmv[0].value
        item["city"] = dmv[1].value
        item["event"] = dmv[2].value
        try:
            item["minutesAgo"] = dmv[3].value
        except:
            pass
        item["event_count"] = mv[0].value
        data.append(item)
    # [END events_data_reformat]

    return data



def pages_data_reformat(response):
    data = []
    # [START events_data_reformat]
    print("Report result:")
    for row in response.rows:
        dmv = row.dimension_values
        mv = row.metric_values
        item = {}
        item["hostName"] = dmv[0].value
        item["citpagePath"] = dmv[1].value
        item["pageTitle"] = dmv[2].value
        
        item["page_count"] = mv[0].value
        data.append(item)
    # [END events_data_reformat]

    return data



def rt_pages_data_reformat(response):
    data = []
    # [START events_data_reformat]
    print("Report result:")
    for row in response.rows:
        dmv = row.dimension_values
        mv = row.metric_values
        item = {}
        item["country"] = dmv[0].value
        item["city"] = dmv[1].value
        item["title"] = dmv[2].value
        try:
            item["minutesAgo"] = dmv[3].value
        except:
            pass
        
        item["page_count"] = mv[0].value
        data.append(item)
    # [END events_data_reformat]

    return data




def print_run_report_response(response):
    """Prints results of a runReport call."""
    # [START analyticsdata_print_run_report_response_header]
    print(f"{response.row_count} rows received")
    for dimensionHeader in response.dimension_headers:
        print(f"Dimension header name: {dimensionHeader.name}")
    for metricHeader in response.metric_headers:
        metric_type = MetricType(metricHeader.type_).name
        print(f"Metric header name: {metricHeader.name} ({metric_type})")
    # [END analyticsdata_print_run_report_response_header]

    # [START analyticsdata_print_run_report_response_rows]
    print("Report result:")
    for row in response.rows:
        for dimension_value in row.dimension_values:
            print(dimension_value.value)

        for metric_value in row.metric_values:
            print(metric_value.value)
    # [END analyticsdata_print_run_report_response_rows]

