#!/usr/bin/env python

# Copyright 2021 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Google Analytics Data API sample application.
"""
from google.analytics.data_v1beta import BetaAnalyticsDataClient
from google.analytics.data_v1beta.types import BatchRunReportsRequest
from google.analytics.data_v1beta.types import RunReportRequest
from google.analytics.data_v1beta.types import DateRange
from google.analytics.data_v1beta.types import Dimension
from google.analytics.data_v1beta.types import Metric
from globals import PROPERTY_ID

from util import general_data_reformat, print_run_report_response


def run_report_with_custom_parameters(property_id="YOUR-GA4-PROPERTY-ID", start_date="2020-01-01", end_date="2020-01-31"):
    """Runs a simple report on a Google Analytics 4 property."""
    client = BetaAnalyticsDataClient()

    # [START run_report_with_custom_parameters]
    request = BatchRunReportsRequest(
            property = f"properties/{property_id}",
            requests = [
                    RunReportRequest(
                        dimensions=[
                            Dimension(name="browser"),
                        ],
                        metrics=[
                            Metric(name="sessions"),
                        ],
                        date_ranges=[DateRange(start_date=start_date, end_date=end_date)],
                    ),
                    RunReportRequest(
                        dimensions=[
                            Dimension(name="country"),
                        ],
                        metrics=[
                            Metric(name="sessions"),
                        ],
                        date_ranges=[DateRange(start_date=start_date, end_date=end_date)],
                    )
                ]
            )
    response = client.batch_run_reports(request)
    # [END run_report_with_custom_parameters]

    for report in response.reports:
        print_run_report_response(report)
        data = general_data_reformat(report)
        print(data)
    

if __name__ == "__main__":
    # TODO(developer): Replace this variable with your Google Analytics 4
    #  property ID before running the sample.
    property_id = PROPERTY_ID
    start_date = "2022-02-11"
    end_date = "2023-02-17"
    run_report_with_custom_parameters(property_id, start_date, end_date)
    